﻿using System;
using Proligence.Profiler.Data;
using StackExchange.Profiling;
using StackExchange.Profiling.Storage;

namespace Proligence.Profiler.Configuration
{
    public class DefaultProfilerConfiguration : IProfilerConfiguration
    {
        public void SetConfiguration()
        {
            WebRequestProfilerProvider.Settings.UserProvider = new IpAddressIdentity();
            MiniProfiler.Settings.SqlFormatter = new PoorMansTSqlFormatter();
            MiniProfiler.Settings.Storage = new HttpRuntimeCacheStorage(TimeSpan.FromMinutes(15));
            MiniProfiler.Settings.MaxJsonResponseSize = int.MaxValue;
            MiniProfiler.Settings.StackMaxLength = 500;
            MiniProfiler.Settings.ExcludeAssembly("MiniProfiler");
            MiniProfiler.Settings.ExcludeAssembly("NHibernate");
        }
    }
}