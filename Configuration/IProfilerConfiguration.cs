﻿using Orchard;

namespace Proligence.Profiler.Configuration
{
    public interface IProfilerConfiguration : ISingletonDependency
    {
        void SetConfiguration();
    }
}