﻿using NHibernate;
using NHibernate.AdoNet;
using NHibernate.AdoNet.Util;
using NHibernate.Exceptions;
using NHibernate.Util;
using Orchard.Environment.Extensions;
using StackExchange.Profiling.Data;
using System;
using System.Data;
using System.Data.Common;
using System.Text;

namespace Proligence.Profiler.Data.AdoNet
{
    [OrchardFeature("Proligence.Profiler.Database")]
    public class ProfiledSqlClientBatchingBatcher : AbstractBatcher
    {
        private readonly int _defaultTimeout;
        private readonly IDbProfiler _profiler;
        private int _batchSize;
        private int _totalExpectedRowsAffected;
        private SqlClientSqlCommandSet _currentBatch;
        private StringBuilder _currentBatchCommandsLog;

        public ProfiledSqlClientBatchingBatcher(ConnectionManager connectionManager, IInterceptor interceptor)
            : base(connectionManager, interceptor)
        {
            _batchSize = Factory.Settings.AdoBatchSize;
            _defaultTimeout = PropertiesHelper.GetInt32(NHibernate.Cfg.Environment.CommandTimeout, NHibernate.Cfg.Environment.Properties, -1);

            _currentBatch = CreateConfiguredBatch();

            // we always create this, because we need to deal with a scenario in which
            // the user change the logging configuration at runtime. Trying to put this
            // behind an if(log.IsDebugEnabled) will cause a null reference exception 
            // at that point.
            _currentBatchCommandsLog = new StringBuilder().AppendLine("Batch commands:");
            _profiler = StackExchange.Profiling.MiniProfiler.Current;
        }

        public override int BatchSize
        {
            get { return _batchSize; }
            set { _batchSize = value; }
        }

        protected override int CountOfStatementsInCurrentBatch
        {
            get { return _currentBatch.CountOfCommands; }
        }

        public override void AddToBatch(IExpectation expectation)
        {
            _totalExpectedRowsAffected += expectation.ExpectedRowCount;
            var batchUpdate = CurrentCommand;
            Driver.AdjustCommand(batchUpdate);

            string lineWithParameters = null;
            var sqlStatementLogger = Factory.Settings.SqlStatementLogger;
            if (sqlStatementLogger.IsDebugEnabled || Log.IsDebugEnabled)
            {
                lineWithParameters = sqlStatementLogger.GetCommandLineWithParameters(batchUpdate);
                var formatStyle = sqlStatementLogger.DetermineActualStyle(FormatStyle.Basic);
                lineWithParameters = formatStyle.Formatter.Format(lineWithParameters);
                _currentBatchCommandsLog.Append("command ")
                    .Append(_currentBatch.CountOfCommands)
                    .Append(":")
                    .AppendLine(lineWithParameters);
            }
            if (Log.IsDebugEnabled)
            {
                Log.Debug("Adding to batch:" + lineWithParameters);
            }

            if (batchUpdate is ProfiledSqlCommand)
            {
                var sqlCommand = ((ProfiledSqlCommand)batchUpdate).SqlCommand;
                _currentBatch.Append(sqlCommand);
                if (_profiler != null)
                {
                    _profiler.ExecuteStart(sqlCommand, ExecuteType.NonQuery);
                }
            }
            else
            {
                _currentBatch.Append((System.Data.SqlClient.SqlCommand)batchUpdate);
            }

            if (_currentBatch.CountOfCommands >= _batchSize)
            {
                ExecuteBatchWithTiming(batchUpdate);
            }
        }

        protected override void DoExecuteBatch(IDbCommand ps)
        {
            Log.DebugFormat("Executing batch");
            CheckReaders();
            Prepare(_currentBatch.BatchCommand);
            if (Factory.Settings.SqlStatementLogger.IsDebugEnabled)
            {
                Factory.Settings.SqlStatementLogger.LogBatchCommand(_currentBatchCommandsLog.ToString());
                _currentBatchCommandsLog = new StringBuilder().AppendLine("Batch commands:");
            }

            if (_profiler != null)
            {
                _profiler.ExecuteStart(_currentBatch.BatchCommand, ExecuteType.NonQuery);
            }

            int rowsAffected;
            try
            {
                rowsAffected = _currentBatch.ExecuteNonQuery();
            }
            catch (DbException e)
            {
                throw ADOExceptionHelper.Convert(Factory.SQLExceptionConverter, e, "could not execute batch command.");
            }

            if (_profiler != null)
            {
                _profiler.ExecuteFinish(_currentBatch.BatchCommand, ExecuteType.NonQuery, null);
            }

            Expectations.VerifyOutcomeBatched(_totalExpectedRowsAffected, rowsAffected);

            _currentBatch.Dispose();
            _totalExpectedRowsAffected = 0;
            _currentBatch = CreateConfiguredBatch();
        }

        private SqlClientSqlCommandSet CreateConfiguredBatch()
        {
            var result = new SqlClientSqlCommandSet();
            if (_defaultTimeout > 0)
            {
                try
                {
                    result.CommandTimeout = _defaultTimeout;
                }
                catch (Exception e)
                {
                    if (Log.IsWarnEnabled)
                    {
                        Log.Warn(e.ToString());
                    }
                }
            }

            return result;
        }
    }
}