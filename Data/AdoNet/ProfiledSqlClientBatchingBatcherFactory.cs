﻿using NHibernate;
using NHibernate.AdoNet;
using NHibernate.Engine;
using Orchard.Environment.Extensions;

namespace Proligence.Profiler.Data.AdoNet
{
    [OrchardFeature("Proligence.Profiler.Database")]
    public class ProfiledSqlClientBatchingBatcherFactory : SqlClientBatchingBatcherFactory
    {
        public override IBatcher CreateBatcher(ConnectionManager connectionManager, IInterceptor interceptor)
        {
            return new ProfiledSqlClientBatchingBatcher(connectionManager, interceptor);
        }
    }
}