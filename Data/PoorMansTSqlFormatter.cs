﻿using Orchard.Environment.Extensions;
using PoorMansTSqlFormatterLib;
using PoorMansTSqlFormatterLib.Formatters;
using StackExchange.Profiling;
using StackExchange.Profiling.SqlFormatters;

namespace Proligence.Profiler.Data
{
    [OrchardFeature("Proligence.Profiler.Database")]
    public class PoorMansTSqlFormatter : ISqlFormatter
    {
        public string FormatSql(SqlTiming timing)
        {
            var sqlFormatter = new SqlServerFormatter();
            var sqlFormat = sqlFormatter.FormatSql(timing);

            var poorMansFormatter = new TSqlStandardFormatter();
            var fullFormatter = new SqlFormattingManager(poorMansFormatter);
            return fullFormatter.Format(sqlFormat);
        }
    }
}