﻿using Orchard.Environment.Extensions;
using StackExchange.Profiling.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Proligence.Profiler.Data
{
    [OrchardFeature("Proligence.Profiler.Database")]
    public class ProfiledSqlCommand : ProfiledDbCommand
    {
        private readonly SqlCommand _sqlCommand;

        public ProfiledSqlCommand(DbCommand command, IDbProfiler profiler)
            : base(command, null, profiler)
        {
            _sqlCommand = (SqlCommand)command;
        }

        public SqlCommand SqlCommand
        {
            get { return _sqlCommand; }
        }
    }
}