﻿using FluentNHibernate.Cfg.Db;
using JetBrains.Annotations;
using Orchard.Data.Providers;
using Orchard.Environment.Extensions;
using System.Diagnostics;

namespace Proligence.Profiler.Data.Providers
{
    [OrchardFeature("Proligence.Profiler.Database")]
    [OrchardSuppressDependency("Orchard.Data.Providers.MySqlDataServicesProvider"), UsedImplicitly]
    public class ProfiledMySqlServerDataServicesProvider : MySqlDataServicesProvider
    {
        public ProfiledMySqlServerDataServicesProvider(string dataFolder, string connectionString)
            : base(dataFolder, connectionString)
        {
        }

        public new static string ProviderName
        {
            get { return MySqlDataServicesProvider.ProviderName; }
        }

        public override IPersistenceConfigurer GetPersistenceConfigurer(bool createDatabase)
        {
            var persistence = (MySQLConfiguration)base.GetPersistenceConfigurer(createDatabase);
            Debug.WriteLine("[Proligence.Profiler] - ProfiledMySqlDataServicesProvider - GetPersistenceConfigurer ");
            return persistence.Driver(typeof(ProfiledMySqlClientDriver).AssemblyQualifiedName);
        }
    }
}