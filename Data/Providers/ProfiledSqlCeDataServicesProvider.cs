﻿using FluentNHibernate.Cfg.Db;
using JetBrains.Annotations;
using Orchard.Data.Providers;
using Orchard.Environment.Extensions;
using MsSqlCeConfiguration = Orchard.Data.Providers.MsSqlCeConfiguration;

namespace Proligence.Profiler.Data.Providers
{
    [OrchardFeature("Proligence.Profiler.Database")]
    [OrchardSuppressDependency("Orchard.Data.Providers.SqlCeDataServicesProvider"), UsedImplicitly]
    public class ProfiledSqlCeDataServicesProvider : SqlCeDataServicesProvider
    {
        public ProfiledSqlCeDataServicesProvider(string dataFolder, string connectionString)
            : base(dataFolder, connectionString)
        {
        }

        public new static string ProviderName
        {
            get { return SqlCeDataServicesProvider.ProviderName; }
        }

        public override IPersistenceConfigurer GetPersistenceConfigurer(bool createDatabase)
        {
            var persistence = (MsSqlCeConfiguration)base.GetPersistenceConfigurer(createDatabase);
            return persistence.Driver(typeof(ProfiledSqlServerCeDriver).AssemblyQualifiedName);
        }
    }
}