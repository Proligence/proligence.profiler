﻿using NHibernate.AdoNet;
using NHibernate.Driver;
using Orchard.Environment.Extensions;
using System;
using System.Data;
using System.Data.Common;
using Proligence.Profiler.Data.AdoNet;

namespace Proligence.Profiler.Data.Providers
{
    [OrchardFeature("Proligence.Profiler.Database")]
    public class ProfiledSqlClientDriver : SqlClientDriver, IEmbeddedBatcherFactoryProvider
    {
        Type IEmbeddedBatcherFactoryProvider.BatcherFactoryClass
        {
            get { return typeof(ProfiledSqlClientBatchingBatcherFactory); }
        }

        public override IDbCommand CreateCommand()
        {
            var command = base.CreateCommand();
            if (StackExchange.Profiling.MiniProfiler.Current != null)
            {
                command = new ProfiledSqlCommand(
                    (DbCommand)command,
                    StackExchange.Profiling.MiniProfiler.Current);
            }

            return command;
        }
    }
}