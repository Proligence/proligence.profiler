﻿using Orchard.Data.Providers;
using Orchard.Environment.Extensions;
using StackExchange.Profiling.Data;
using System.Data;
using System.Data.Common;

namespace Proligence.Profiler.Data.Providers
{
    [OrchardFeature("Proligence.Profiler.Database")]
    public class ProfiledSqlServerCeDriver : SqlCeDataServicesProvider.OrchardSqlServerCeDriver
    {
        public override IDbCommand CreateCommand()
        {
            var command = base.CreateCommand();
            if (StackExchange.Profiling.MiniProfiler.Current != null)
            {
                command = new ProfiledDbCommand(
                    (DbCommand)command,
                    (ProfiledDbConnection)command.Connection,
                    StackExchange.Profiling.MiniProfiler.Current);
            }

            return command;
        }

        public override IDbConnection CreateConnection()
        {
            if (StackExchange.Profiling.MiniProfiler.Current == null)
            {
                return base.CreateConnection();
            }

            return new ProfiledDbConnection(
                base.CreateConnection() as DbConnection,
                StackExchange.Profiling.MiniProfiler.Current);
        }
    }
}