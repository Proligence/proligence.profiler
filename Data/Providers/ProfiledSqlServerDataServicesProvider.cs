﻿using FluentNHibernate.Cfg.Db;
using JetBrains.Annotations;
using Orchard.Data.Providers;
using Orchard.Environment.Extensions;
using System.Diagnostics;

namespace Proligence.Profiler.Data.Providers
{
    [OrchardFeature("Proligence.Profiler.Database")]
    [OrchardSuppressDependency("Orchard.Data.Providers.SqlServerDataServicesProvider"), UsedImplicitly]
    public class ProfiledSqlServerDataServicesProvider : SqlServerDataServicesProvider
    {
        public ProfiledSqlServerDataServicesProvider(string dataFolder, string connectionString)
            : base(dataFolder, connectionString)
        {
        }

        public static new string ProviderName
        {
            get { return SqlServerDataServicesProvider.ProviderName; }
        }

        public override IPersistenceConfigurer GetPersistenceConfigurer(bool createDatabase)
        {
            var persistence = (MsSqlConfiguration)base.GetPersistenceConfigurer(createDatabase);
            Debug.WriteLine("[Proligence.Profiler] - ProfiledSqlServerDataServicesProvider - GetPersistenceConfigurer ");
            return persistence.Driver(typeof(ProfiledSqlClientDriver).AssemblyQualifiedName);
        }
    }
}