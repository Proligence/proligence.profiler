﻿using JetBrains.Annotations;
using Orchard;
using Orchard.DisplayManagement;
using Orchard.Environment.Extensions;
using Orchard.Mvc.Filters;
using Orchard.Security;
using System.Linq;
using System.Web.Mvc;
using Proligence.Profiler.Services;

namespace Proligence.Profiler.Filters
{
    /// <summary>
    /// Filter for injecting profiler view code.
    /// </summary>
    [OrchardFeature("Proligence.Profiler")]
    [UsedImplicitly]
    public class ProfilerFilter : FilterProvider, IResultFilter, IActionFilter
    {
        private readonly IAuthorizer _authorizer;
        private readonly dynamic _shapeFactory;
        private readonly WorkContext _workContext;
        private readonly IProfilerService _profiler;

        public ProfilerFilter(WorkContext workContext, IAuthorizer authorizer, IShapeFactory shapeFactory, IProfilerService profiler)
        {
            _workContext = workContext;
            _shapeFactory = shapeFactory;
            _authorizer = authorizer;
            _profiler = profiler;
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            _profiler.StepStop(StepKeys.ActionFilter);
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var tokens = filterContext.RouteData.DataTokens;
            var area = tokens.ContainsKey("area") && !string.IsNullOrEmpty(tokens["area"].ToString()) ?
                string.Concat(tokens["area"], ".") :
                string.Empty;
            var controller = string.Concat(filterContext.Controller.ToString().Split('.').Last(), ".");
            var action = filterContext.ActionDescriptor.ActionName;

            _profiler.StepStart(StepKeys.ActionFilter, "Action: " + area + controller + action);
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            // should only run on a full view rendering result
            if (!(filterContext.Result is ViewResult))
            {
                return;
            }

            if (!IsActivable())
            {
                return;
            }

            _profiler.StepStop(StepKeys.ResultFilter);
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            // should only run on a full view rendering result
            if (!(filterContext.Result is ViewResult))
            {
                return;
            }

            if (!IsActivable())
            {
                return;
            }

            var place = _workContext.Layout.Footer ?? _workContext.Layout.Head;
            place.Add(_shapeFactory.MiniProfilerTemplate());

            _profiler.StepStart(StepKeys.ResultFilter, string.Format("Result: {0}", filterContext.Result));
        }

        private bool IsActivable()
        {
            // if not logged as a site owner, still activate if it's a local request (development machine)
            return _authorizer.Authorize(StandardPermissions.SiteOwner) || _workContext.HttpContext.Request.IsLocal;
        }
    }
}