﻿using JetBrains.Annotations;
using Orchard.ContentManagement.Records;

namespace Proligence.Profiler.Models
{
    /// <summary>
    /// Dummy record for including this module in data configuring.
    /// </summary>
    [UsedImplicitly]
    public class DummyRecord : ContentPartRecord
    {
    }
}