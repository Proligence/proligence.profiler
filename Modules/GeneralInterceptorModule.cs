﻿using System;
using System.Dynamic;
using Autofac;
using Autofac.Core;
using Castle.DynamicProxy;
using JetBrains.Annotations;
using Orchard;
using Orchard.ContentManagement.Drivers;
using Orchard.Data.Migration;
using Orchard.Environment.AutofacUtil.DynamicProxy2;
using Orchard.Environment.Extensions;
using Orchard.Events;
using Proligence.Profiler.Services;

namespace Proligence.Profiler.Modules
{
    [OrchardFeature("Proligence.Profiler.Components")]
    [UsedImplicitly]
    public class GeneralInterceptorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GeneralInterceptor>().AsImplementedInterfaces().AsSelf().InstancePerLifetimeScope();
        }

        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            var context = DynamicProxyContext.From(registration);
            if (context != null)
            {
                // Need to filter out IDataMigration implementations - those are heavily reflected on
                if (registration.Activator.LimitType.Namespace != null
                    && !registration.Activator.LimitType.Namespace.StartsWith("Orchard.Data")
                    && !registration.Activator.LimitType.Namespace.StartsWith("Proligence.Profiler")
                    && IsValidType(registration.Activator.LimitType)
                    && !typeof(IDataMigration).IsAssignableFrom(registration.Activator.LimitType)
                    && !typeof(IInterceptor).IsAssignableFrom(registration.Activator.LimitType)
                    && !typeof(Module).IsAssignableFrom(registration.Activator.LimitType))
                {
                    registration.InterceptedBy<GeneralInterceptor>();
                }
            }
        }

        internal static bool IsValidType(Type type)
        {
            return typeof(IDependency).IsAssignableFrom(type)
                && !typeof(IContentPartDriver).IsAssignableFrom(type) 
                && !typeof(IEventHandler).IsAssignableFrom(type) 
                && !typeof(DynamicObject).IsAssignableFrom(type);
        }
    }
}