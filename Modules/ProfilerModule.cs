﻿using Autofac;
using Autofac.Core;
using JetBrains.Annotations;
using Orchard;
using Orchard.Environment.Extensions;
using Proligence.Profiler.Services;

namespace Proligence.Profiler.Modules
{
    [OrchardFeature("Proligence.Profiler")]
    [UsedImplicitly]
    public class ProfilerModule : Module
    {
        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            if (registration.Activator.LimitType == typeof(WorkContext))
            {
                registration.Activated += (sender, args) => args.Context.Resolve<IProfilerService>();
            }
        }

    }
}