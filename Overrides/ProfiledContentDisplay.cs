﻿using System;
using System.Collections.Generic;
using System.Web.Routing;
using JetBrains.Annotations;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.DisplayManagement;
using Orchard.DisplayManagement.Descriptors;
using Orchard.FileSystems.VirtualPath;
using Orchard.Logging;
using Orchard.Environment.Extensions;
using Proligence.Profiler.Services;

namespace Proligence.Profiler.Overrides {
    [OrchardFeature("Proligence.Profiler.Display")]
    [OrchardSuppressDependency("Orchard.ContentManagement.DefaultContentDisplay"), UsedImplicitly]
    public class ProfiledContentDisplay : IContentDisplay {
        private readonly IProfilerService _profiler;
        private readonly DefaultContentDisplay _innerContentDisplay;

        public ProfiledContentDisplay(
            Lazy<IEnumerable<IContentHandler>> handlers,
            IShapeFactory shapeFactory,
            Lazy<IShapeTableLocator> shapeTableLocator, 
            RequestContext requestContext,
            IVirtualPathProvider virtualPathProvider,
            IWorkContextAccessor workContextAccessor,
            IProfilerService profiler)
        {

            _innerContentDisplay = new DefaultContentDisplay(
                handlers,
                shapeFactory,
                shapeTableLocator,
                requestContext,
                virtualPathProvider,
                workContextAccessor);

            _profiler = profiler;
        }

        public ILogger Logger
        {
            get { return _innerContentDisplay.Logger; }
            set { _innerContentDisplay.Logger = value; }
        }

        public dynamic BuildDisplay(IContent content, string displayType, string groupId) {
            _profiler.StepStart(StepKeys.ContentDisplay, "BuildDisplay (" + (string.IsNullOrEmpty(displayType) ? "Detail" : displayType) + "): " + content.Id + "(" + content.ContentItem.ContentType + ")");
            var result = _innerContentDisplay.BuildDisplay(content, displayType, groupId);
            _profiler.StepStop(StepKeys.ContentDisplay);
            return result;
        }

        public dynamic BuildEditor(IContent content, string groupId) {
            _profiler.StepStart(StepKeys.ContentDisplay, "BuildEditor: " + content.Id + "(" + content.ContentItem.ContentType + ")");
            var result = _innerContentDisplay.BuildEditor(content, groupId);
            _profiler.StepStop(StepKeys.ContentDisplay);
            return result;
        }

        public dynamic UpdateEditor(IContent content, IUpdateModel updater, string groupInfoId) {
            _profiler.StepStart(StepKeys.ContentDisplay, "UpdateEditor: " + content.Id + "(" + content.ContentItem.ContentType + ")");
            var result = _innerContentDisplay.UpdateEditor(content, updater, groupInfoId);
            _profiler.StepStop(StepKeys.ContentDisplay);
            return result;
        }
    }
}
