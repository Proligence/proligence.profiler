﻿using JetBrains.Annotations;
using Orchard.DisplayManagement.Implementation;
using Orchard.DisplayManagement.Shapes;
using Orchard.ContentManagement;
using Orchard.Environment.Extensions;
using Proligence.Profiler.Services;

namespace Proligence.Profiler.Overrides
{
    [OrchardFeature("Proligence.Profiler.Display"), UsedImplicitly]
    public class ProfiledShapes : IShapeFactoryEvents
    {
        private readonly IProfilerService _profiler;

        public ProfiledShapes(IProfilerService profiler)
        {
            _profiler = profiler;
        }

        public void Creating(ShapeCreatingContext context)
        {
        }

        public void Created(ShapeCreatedContext context)
        {
            var shapeMetadata = (ShapeMetadata)context.Shape.Metadata;
            shapeMetadata.OnDisplaying(OnDisplaying);
            shapeMetadata.OnDisplayed(OnDisplayed);
        }

        public void Displaying(ShapeDisplayingContext context)
        {
            if (context.ShapeMetadata.Type.Equals("Zone"))
            {
                return;
            }

            _profiler.StepStart(StepKeys.Shape, context.ShapeMetadata.Type + " - Display");
        }

        public void Displayed(ShapeDisplayedContext context)
        {
            if (context.ShapeMetadata.Type.Equals("Zone"))
            {
                return;
            }

            _profiler.StepStop(StepKeys.Shape);
        }

        public void OnDisplaying(ShapeDisplayingContext context)
        {
            IContent content = null;
            if (context.Shape.ContentItem != null)
            {
                content = context.Shape.ContentItem;
            }
            else if (context.Shape.ContentPart != null)
            {
                content = context.Shape.ContentPart;
            }

            var message = string.Format("Shape: " + context.ShapeMetadata.Type 
                + " (" + (string.IsNullOrWhiteSpace(context.ShapeMetadata.DisplayType) ? "Detail" : context.ShapeMetadata.DisplayType) + "). "
                + (content != null ? "Content: " + content.ContentItem.ContentType : "")
                + ". Sources: " + string.Join(",", context.ShapeMetadata.BindingSources));

            _profiler.StepStart(StepKeys.Shape, message, true);
        }

        public void OnDisplayed(ShapeDisplayedContext context)
        {
            _profiler.StepStop(StepKeys.Shape);
        }
    }
}