﻿using System.Collections.Generic;
using System.Web.Routing;
using JetBrains.Annotations;
using Orchard.Environment.Extensions;
using Orchard.Tokens;
using Orchard.Tokens.Implementation;
using Proligence.Profiler.Services;

namespace Proligence.Profiler.Overrides {
    [OrchardFeature("Proligence.Profiler.Tokens")]
    [OrchardSuppressDependency("Orchard.Tokens.Implementation.Tokenizer"), UsedImplicitly]
    public class ProfiledTokenizer : Tokenizer, ITokenizer {
        private readonly IProfilerService _profiler;

        public ProfiledTokenizer(ITokenManager tokenManager, IProfilerService profiler) : base(tokenManager)
        {
            _profiler = profiler;
        }

        public new IDictionary<string, object> Evaluate(IEnumerable<string> tokens, object data) {
            _profiler.StepStart(StepKeys.Token, "Evaluate token: " + string.Join(",", tokens) + ". Data: " + data);
            var retval = base.Evaluate(tokens, new RouteValueDictionary(data));
            _profiler.StepStop(StepKeys.Token);

            return retval;
        }

        public new IDictionary<string, object> Evaluate(IEnumerable<string> tokens, IDictionary<string, object> data) {
            _profiler.StepStart(StepKeys.Token, "Evaluate token: " + string.Join(",", tokens) + ". Data: " + data);
            var retval = base.Evaluate(tokens, data);
            _profiler.StepStop(StepKeys.Token);

            return retval;
        }

        string ITokenizer.Replace(string text, object data) {
            _profiler.StepStart(StepKeys.Token, "Replace token: " + text);
            var retval = Replace(text, data, ReplaceOptions.Default);
            _profiler.StepStop(StepKeys.Token);

            return retval;
        }

        string ITokenizer.Replace(string text, object data, ReplaceOptions options) {
            _profiler.StepStart(StepKeys.Token, "Replace token: " + text);
            var retval = Replace(text, new RouteValueDictionary(data), options);
            _profiler.StepStop(StepKeys.Token);

            return retval;
        }

        string ITokenizer.Replace(string text, IDictionary<string, object> data) {
            _profiler.StepStart(StepKeys.Token, "Replace token: " + text);
            var retval = Replace(text, data, ReplaceOptions.Default);
            _profiler.StepStop(StepKeys.Token);

            return retval;
        }

        string ITokenizer.Replace(string text, IDictionary<string, object> data, ReplaceOptions options) {
            _profiler.StepStart(StepKeys.Token, "Replace token: " + text);
            var retval = Replace(text, data, options);
            _profiler.StepStop(StepKeys.Token);

            return retval;
        }
    }
}