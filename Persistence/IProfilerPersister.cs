﻿using Orchard;

namespace Proligence.Profiler.Persistence
{
    public interface IProfilerPersister : ISingletonDependency
    {
        void Save(StackExchange.Profiling.MiniProfiler profiler);
    }
}