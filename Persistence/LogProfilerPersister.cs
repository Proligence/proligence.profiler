﻿using Orchard;
using Orchard.Environment.Extensions;
using Orchard.Logging;
using StackExchange.Profiling;

namespace Proligence.Profiler.Persistence
{
    [OrchardFeature("Proligence.Profiler.Logging")]
    public class LogProfilerPersister : Component, IProfilerPersister
    {
        public void Save(StackExchange.Profiling.MiniProfiler profiler)
        {
            if(profiler != null) Logger.Information(profiler.RenderPlainText());
        }
    }
}