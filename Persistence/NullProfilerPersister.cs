﻿using Orchard.Environment.Extensions;

namespace Proligence.Profiler.Persistence
{
    [OrchardFeature("Proligence.Profiler")]
    public class NullProfilerPersister : IProfilerPersister
    {
        public void Save(StackExchange.Profiling.MiniProfiler profiler) { }
    }
}