﻿using System.Linq;
using Castle.DynamicProxy;
using JetBrains.Annotations;
using Orchard.Environment;
using Orchard.Environment.Extensions;
using StackExchange.Profiling;

namespace Proligence.Profiler.Services
{
    [OrchardFeature("Proligence.Profiler.Components")]
    [UsedImplicitly]
    public class GeneralInterceptor : ICallInterceptor
    {
        private readonly Work<IProfilerService> _service;
        private volatile object _syncRoot = new object();

        public GeneralInterceptor(Work<IProfilerService> service)
        {
            _service = service;
        }

        public void Intercept(IInvocation i)
        {
            if (_service.Value == null)
            {
                i.Proceed();
                return;
            }

            var methodCallString = i.GetConcreteMethodInvocationTarget().Name +
                "(" +
                string.Join(", ", i.Arguments.Select(a => a == null ? "null" : a.GetType().FullName == a.ToString() ? "[obj]" : a.ToString())) +
                ")";
            lock (_syncRoot)
            {
                using (_service.Value.Profiler.Step(i.TargetType.Name + ": " + methodCallString, ProfileLevel.Verbose))
                {
                    i.Proceed();
                }
            }
        }
    }

}