﻿using Castle.DynamicProxy;

namespace Proligence.Profiler.Services
{
    public interface ICallInterceptor : IInterceptor
    {}
}