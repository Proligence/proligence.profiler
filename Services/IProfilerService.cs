﻿using System;
using Orchard;

namespace Proligence.Profiler.Services
{
    public interface IProfilerService : IDependency
    {
        void StepStart(string key, string message, bool isVerbose = false);
        void StepStop(string key);

        IDisposable Step(string message);
        StackExchange.Profiling.MiniProfiler Profiler { get; }
    }
}