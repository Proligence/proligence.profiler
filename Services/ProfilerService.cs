﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using Autofac;
using JetBrains.Annotations;
using Orchard.Environment.Extensions;
using Orchard.Mvc;
using Proligence.Profiler.Configuration;
using Proligence.Profiler.Persistence;
using StackExchange.Profiling;

namespace Proligence.Profiler.Services
{
    [OrchardFeature("Proligence.Profiler")]
    [UsedImplicitly]
    public class ProfilerService : IProfilerService, IDisposable
    {
        private readonly ILifetimeScope _scope;
        private readonly IHttpContextAccessor _accessor;
        private readonly IProfilerPersister _persister;
        private readonly ConcurrentDictionary<string, ConcurrentStack<IDisposable>> _steps = new ConcurrentDictionary<string, ConcurrentStack<IDisposable>>();
        private MiniProfiler _profiler;

        public ProfilerService(ILifetimeScope scope, IHttpContextAccessor accessor, IProfilerConfiguration configuration, IProfilerPersister persister)
        {
            _scope = scope;
            _accessor = accessor;
            _persister = persister;

            if (_scope.Tag.Equals("work") && _accessor.Current() != null)
            {
                configuration.SetConfiguration();
                MiniProfiler.Start(ProfileLevel.Verbose);
                _profiler = MiniProfiler.Current;
                Debug.WriteLine("Profiler started");
            }
        }

        public MiniProfiler Profiler
        {
            get { return _profiler ?? (_profiler = MiniProfiler.Current); }
        }

        public void StepStart(string key, string message, bool isVerbose = false)
        {
            if (Profiler == null)
            {
                return;
            }

            var stack = _steps.GetOrAdd(key, k => new ConcurrentStack<IDisposable>());
            var step = Profiler.Step(message, isVerbose ? ProfileLevel.Verbose : ProfileLevel.Info);
            stack.Push(step);
        }

        public void StepStop(string key)
        {
            
            if (Profiler == null)
            {
                return;
            }

            IDisposable step;
            if (_steps[key].TryPop(out step))
            {
                step.Dispose();
            }
        }

        public IDisposable Step(string message)
        {
            return Profiler.Step(message, ProfileLevel.Verbose);
        }

        public void StopAll()
        {
            // Dispose any orphaned steps
            foreach (var stack in _steps.Values)
            {
                IDisposable step;
                while (stack.TryPop(out step))
                {
                    step.Dispose();
                    Debug.WriteLine("[Proligence.Profiler] - ProfilerService - StopAll There is some left");
                }
            }
        }

        public void Dispose()
        {
            var profiler = Profiler;
            if (profiler == null) return;

            StopAll();

            if (_scope.Tag.Equals("work") && _accessor.Current() != null)
            { 
                MiniProfiler.Stop();
                _persister.Save(profiler);
                Debug.WriteLine("Profiler stopped");
            }
        }
    }
}