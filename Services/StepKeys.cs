﻿namespace Proligence.Profiler.Services
{
    public static class StepKeys
    {
        public const string ActionFilter = "ActionFilter";
        public const string ResultFilter = "ResultFilter";
        public const string Shape = "ShapeProfiling";
        public const string Token = "Tokens";
        public const string Driver = "Drivers";
        public const string ContentDisplay = "ContentDisplay";
        public const string EventBus = "EventBus";
    }
}